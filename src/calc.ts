
export function sum(...args: Array<number>): number {
    return args.reduce((acc, current) => {
        return acc + current;
    }, 0);
}

export function multiplier(multi: number, ...args: number[]): number[] {
    return args.map((item) => {
        return item * multi;
    });
}

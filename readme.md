<h1 align="center">Welcome to start_jest 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: ISC" src="https://img.shields.io/badge/License-ISC-yellow.svg" />
  </a>
</p>

> TS project to introduce jest and test methods

### 🏠 [Homepage](https://github.com/nadavadan)




## Run tests

```sh
npm run test-w
```

## Author

👤 **Nadav Tal**

* Github: [@nadavadan](https://github.com/nadavadan)
* LinkedIn: [@Nadav Tal](https://linkedin.com/in/Nadav Tal)

## Show your support

Give a ⭐️ if this project helped you!

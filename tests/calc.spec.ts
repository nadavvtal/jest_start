import { expect } from "chai";
import { sum,multiplier } from "../src/calc";


describe("Testing calc module", () => {


    context("#sum",()=>{

        it("Should be 5", () => {
            expect(sum(2, 3)).to.equal(5);
        });

        it("Should be 21", () => {
            expect(sum(1,2,3,4,5,6)).to.equal(21);
        });
    
        it("should be a function", () => {
            expect(sum).to.be.a("function");
            expect(sum).to.be.a.instanceOf(Function);
        });

    }),
// ------------------------------------------------------------------------------------------------------------
    
context("#multiplier",()=>{

        it("Should be [6]", () => {
            expect(multiplier(2, 3)).to.eql([6]);
        });
  
        it("Should be [12,15,18]", () => {
            expect(multiplier(3,4,5,6)).to.eql([12,15,18]);
        });

    
        it("should be a function", () => {
            expect(multiplier).to.be.a("function");
            expect(multiplier).to.be.a.instanceOf(Function);
        });
    
    });
});
